export default (customElement) => { 
    switch(customElement.constructor) {
      case HTMLElement: return false; 
      case HTMLUnknownElement: return undefined; 
    }
    return true;
}