const routesMock = [
	'/:',
	'/candidate-detail/:idCandidate',
	'/candidates:',
	'/offer-detail/:idOffer:',
	'/offers:',
	'/selection-detail:'
];


export {routesMock};
