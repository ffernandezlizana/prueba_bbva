function router(routes, errorUrl = '/error404') {
	const routesPlus = getRoutesPlus(routes);
	return matchRoute(routesPlus, errorUrl);
  }

  function getRoutesPlus(routes) {
	const routesPlus = {...routes};
	const routesKeys = Object.keys(routesPlus);

	routesKeys.forEach((route) => {
	  const routeObject = {
		cb: routesPlus[route],
		segmentsArray: route.split('/'),
	  };

	  routeObject.segmentsArray.shift();

	  routesPlus[route] = routeObject;
	});
	return routesPlus;
  }

  function matchRoute(routesPlus, errorUrl) {
	const urlSegments = getUrlFragmentSegments();
	let routesKeys = Object.keys(routesPlus);

	urlSegments.forEach((segment, idxSegment) => {
	  routesKeys = routesKeys.filter((route) => {
		const segmentsArray = routesPlus[route].segmentsArray;
		return urlSegments.length === segmentsArray.length && (segment === segmentsArray[idxSegment] || segmentsArray[idxSegment].charAt(0) === ':');
	  });
	});

	let idValuesObj = Object.create(null, {});
	if (routesKeys[0]) {
	  idValuesObj = getIdValues(routesPlus[routesKeys[0]].segmentsArray, urlSegments);
	}

	return [routesKeys[0] || errorUrl, idValuesObj];
  }

  function getIdValues(segmentsArray, urlSegments) {
	const idValuesObj = Object.create(null, {});

	segmentsArray.forEach((elem, idx) => {
	  if (elem.charAt(0) === ':') {
		idValuesObj[elem.substring(1)] = urlSegments[idx];
	  }
	});

	return idValuesObj;
  }

  function getUrlFragmentSegments() {
	const url = window.location.hash.slice(1).toLowerCase() || '/';
	const urlSegments = url.split('/');
	urlSegments.shift();
	if (urlSegments[urlSegments.length - 1] === '') {
	  urlSegments.pop();
	}
	return urlSegments;
  }

  export default router;
