import router from './router.js';

function initRouter() {
	window.addEventListener('load', route);
	window.addEventListener('hashchange', route);
}

async function route() {
	const routes = {
		'/': goToInitPage,
		'/secondPage': goToSecondPage,
	};
	matchRoute(routes);
}

function matchRoute(routes) {
		const [matchedRouteKey, idValuesObj] = router(routes);
		routes[matchedRouteKey](idValuesObj);
}

function goToInitPage() {
	const $container = document.querySelector('main');
	$container.innerHTML = '<main-example></main-example>';
}

function goToSecondPage() {
	const $container = document.querySelector('main');
	$container.innerHTML = '<second-page-example></second-page-example>';
}

function goTo(url) {
	window.location.hash = url;
}

export {initRouter, goTo};
