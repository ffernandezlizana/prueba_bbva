/* eslint-disable import/order */
import './app/define.components.js';
import './assets/scss/main.scss';
import {initRouter} from './routes.js';

async function init() {
	initRouter();
}

init();
