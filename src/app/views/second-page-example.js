/* eslint-disable no-useless-constructor */
/* eslint-disable class-methods-use-this */
/* eslint-disable no-restricted-globals */
class SecondPageExample extends HTMLElement {
	static get tag() {
		return 'second-page-example';
	}

	constructor() {
		super();
	}

	disconnectedCallback() {
		this.remove();
	}

	connectedCallback() {
		this.render();
		this.initComponent();
	}

	render() {
		this.innerHTML = `
			${this.templateCSS()}
			${this.template()}
		`;
	}

	templateCSS() {
		return `
		<style>
			.examples-pages{
				background-color: var(--color-dark-navy);
				color: var(--color-white);
				padding: 20px 50px;
			}
			.examples-pages h2{
				color: var(--color-white);
			}
			.section{
				padding: 30px
			}
			.dot {
				color: var(--color-coral);
				font-size: xx-large;
			}
		</style>
		`;
	}

	template() {
		return /* html */`
		<div class="examples-pages">
			<h2>Segunda página con router<span class="dot">.</span>
			</h2>
		</div>
		<div class="section">
			<div class="card">
				<p class="form-label">
						Esta es la segunda página de ejemplo, navegando con el router desde el fichero routes.js (insertando componentenes dentro de la etiqueta <code> &lt;main>&lt;/main> </code>
				</p><br>
				<core-button
				link coral small
				id="landing"
				text="Volver a página principal"
			>
			</div>
			</div>
	`;
	}

	initComponent() {
		this.querySelector('#landing').addEventListener('on-click-core-button', () => {
			history.pushState({param1: null, param2: null}, 'landing', '#');
		});
	}
}

export default SecondPageExample;
