/* eslint-disable no-useless-constructor */
/* eslint-disable class-methods-use-this */
/* eslint-disable no-restricted-globals */

class MainExample extends HTMLElement {
	static get tag() {
		return 'main-example';
	}

	constructor() {
		super();
	}

	disconnectedCallback() {
		this.remove();
	}

	connectedCallback() {
		this.render();
		this.initComponent();
	}

	render() {
		this.innerHTML = `
			${this.templateCSS()}
			${this.template()}
		`;
	}

	templateCSS() {
		return `
		<style>
			.examples-pages{
				background-color: var(--color-dark-navy);
				color: var(--color-white);
				padding: 20px 50px;
			}
			.examples-pages h2{
				color: var(--color-white);
			}
			.section{
				padding: 30px
			}
			.dot {
				color: var(--color-coral);
				font-size: xx-large;
			}
		</style>
		`;
	}

	template() {
		return /* html */ `
			<div class="examples-pages">
				<h2>Página Principal<span class="dot">.</span>
				</h2>
			</div>
			<div class="section">
				<div class="card">
					<p class="form-label">
						Este es un ejemplo de como navegar por el router, se inicializa en el fichero main.js, donde definimos en el rutes.js todas las rutas,
						y los componentes qu se mostrarán al entrar en dicha ruta
					</p><br>


					<core-button
						link coral small
						id="second"
						text="Ir a página secundaria a través del router">
					</core-button>
					<br>
					<p class="form-label">
						En el caso que nuestra aplicación no tenga rotuer, habría que eliminar la inicializacion en el main.js y eliminar el fichero routes.js
					</p>

				</div>
			</div>

			<div class="section">
				<div class="card">
					<h2>Librería core-ui</h2><br>
					<p class="form-label">
						Encontrarás toda la documentación de core-ui en el siguiente enlace.
					</p>
					<core-button
					id="core-ui"
					coral link
					small
					title="Abrir Doc core-iu"
					text="Abrir Doc core-iu"
					></core-button>
				</div>
				</div>

		`;
	}

	initComponent() {
		this.querySelector('#core-ui').addEventListener(
			'on-click-core-button',
			() => {
				window.open('http://core-ui.paradigmadigital.com/', '_blank');
			},
		);

		this.querySelector('#second').addEventListener(
			'on-click-core-button',
			() => {
				history.pushState(
					{param1: null, param2: null},
					'second Page',
					'#/secondPage',
				);
			},
		);
	}
}

export default MainExample;
