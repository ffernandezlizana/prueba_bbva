// Componentes app
import MainExample from './views/main-example.js';
import SecondPageExample from './views/second-page-example.js';

window.customElements.define('main-example', MainExample);
window.customElements.define('second-page-example', SecondPageExample);
