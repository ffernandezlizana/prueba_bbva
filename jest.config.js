const jestConfig = {
  verbose: true,
  runner: '@jest-runner/electron',
  testEnvironment: '@jest-runner/electron/environment',
  transform: {
    "^.+\\.js$": "babel-jest",
  },
  moduleDirectories: ['node_modules', 'test'],
  testPathIgnorePatterns: [
    '<rootDir>/.*/dist/',
    '<rootDir>/dist/',
    '<rootDir>/cypress/',
  ],
  transformIgnorePatterns: [
    '<rootDir>/node_modules/',
    '<rootDir>/cypress/',

  ],
  "modulePaths": [
    "<rootDir>",
  ],
};
module.exports = jestConfig;